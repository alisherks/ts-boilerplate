module.exports = {
    "extends": [
        "./tslint.config.js"
    ],
    "rules": {
        "no-console": 1,
        "no-debugger": 1
    }
};