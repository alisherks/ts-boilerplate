import './styles/style.scss';
import 'font-awesome';

if ((module as any).hot) {
    (module as any).hot.accept();
}

document.body.innerHTML = require('./content.html');