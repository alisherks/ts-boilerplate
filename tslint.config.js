module.exports = {
    "env": {
        "browser": true,
        "node": true
    },
    "parserOptions": {
        "ecmaVersion": 6,
        "sourceType": "module",
        "ecmaFeatures": {
            "jsx": true
        }
    },
    "extends": [
        "tslint:recommended",
        "tslint-eslint-rules"
    ],
    "rules": {
        "no-eval": 2,
        "prefer-const": 1
    }
};